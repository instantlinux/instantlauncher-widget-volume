declare module "loudness" {
    export function setVolume(volume: number, cb: any): any;
    export function getVolume(cb: any): any;
}