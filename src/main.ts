import * as loudness from 'loudness';

export default function createWidget(widgetfolder: string) {
    // Create Range-Input.
    var range = document.createElement("input");
    range.type = "range";
    range.min = "1";
    range.max = "100";
    range.value = "50";

    // Set volume.
    range.oninput = function() {
        loudness.setVolume(parseInt(range.value), function (err: any) {
            // Done
        });
    }

    // Get volume.
    loudness.getVolume(function (err: any, vol: number) {
        // Set current value.
        range.value = vol.toString();
    });


    // STYLE
    // General
    range.style.webkitAppearance = "none";
    range.style.background       = "transparent";
    range.style.width            = "80%";
    range.style.position         = "absolute";
    range.style.top              = "50%";
    range.style.left             = "50%";
    range.style.transform        = "translate(-50%, -50%)";
    range.style.outline          = "none";
    // Slider Thumb
    var thumbstyle = document.createElement("style");
    thumbstyle.innerHTML = "input[type=\"range\"]::-webkit-slider-thumb {\n";
    thumbstyle.innerHTML += "    -webkit-appearance: none;\n";
    thumbstyle.innerHTML += "    height: 30px;\n";
    thumbstyle.innerHTML += "    width: 30px;\n";
    thumbstyle.innerHTML += "    border-radius: 100%;\n";
    thumbstyle.innerHTML += "    background: #1abc9c;\n";
    thumbstyle.innerHTML += "    cursor: pointer;\n";
    thumbstyle.innerHTML += "    margin-top: -12px;\n";
    thumbstyle.innerHTML += "}";
    range.appendChild(thumbstyle);
    // Slider track
    var trackstyle = document.createElement("style");
    trackstyle.innerHTML = "input[type=\"range\"]::-webkit-slider-runnable-track {\n";
    trackstyle.innerHTML += "    height: 8px;\n";
    trackstyle.innerHTML += "    cursor: pointer;\n";
    trackstyle.innerHTML += "    background: rgba(250, 250, 250, 0.6);\n";
    trackstyle.innerHTML += "    border-radius: 8px;\n";
    trackstyle.innerHTML += "}";
    range.appendChild(trackstyle);

    setTimeout(updateVolume, 500, range);

    return range;
}

function updateVolume(element: HTMLInputElement) {
    console.log("Updating volume...");
    // Get volume.
    loudness.getVolume(function (err: any, vol: number) {
        // Set current value.
        element.value = vol.toString();
    });

    setTimeout(updateVolume, 500, element)
}