# InstantLauncher widget - Volume

> Widget for [InstantLauncher](https://github.com/InstantCodee/instantLauncher)

![Screenshot](screenshot.png?raw=true "Screenshot")

## What does it do?

> This widget displays an alsa-based volume control.

## Release History

* Initial Commit

## Meta

Hannes Schulze – [guidedlinux.org](https://www.guidedlinux.org/) – projects@guidedlinux.org

Distributed under the GPL-3.0 license. See ``LICENSE`` for more information.

[https://github.com/guidedlinux/](https://github.com/guidedlinux/)

## Contributing

1. Fork it (<https://github.com/InstantCodee/instantlauncher-widget-volume.git>)
1. Create your feature branch (`git checkout -b feature/fooBar`)
1. Commit your changes (`git commit -am 'Add some fooBar'`)
1. Push to the branch (`git push origin feature/fooBar`)
1. Create a new Pull Request
